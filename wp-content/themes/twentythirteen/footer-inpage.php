 <!-- end col-lg-6 -->
      <div class="clearfix"></div>
	 <?php  $images = nggImageList(1);
			foreach ($images as $image){  ?>   
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="client ImageWrapper GrayScale" data-effect="fade"> <a href="<?php echo $image->description;?>"><img src="<?php echo $image->imageURL; ?>" alt=""></a> </div>
      </div>
    <?php } ?>
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>
<!-- end whitewrapper -->
 <footer class="footer1 jt-shadow clearfix">
        	<div class="container">
            	<div class="row">
          			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    	                    	            <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-left-widget') ) ?>

                    </div><!-- end col-lg-3 -->
          			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    	<div class="widget clearfix">
                        	<div class="title">
                            	<h3>Contact Info</h3>
                            </div><!-- end title -->
                            <div class="contact-widget">
                              <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-center-widget') ) ?>

                                <ul class="social clearfix">
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook" title="" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Google Plus" title="" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter" title="" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Youtube" title="" href="#"><i class="fa fa-youtube"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Linkedin" title="" href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Dribbble" title="" href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Skype" title="" href="#"><i class="fa fa-skype"></i></a></li>
                                </ul><!-- end social -->
                            </div><!-- contact-widget -->
                        </div><!-- end widget -->
                    </div><!-- end col-lg-3 -->
          			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    	 <div class="widget clearfix">
                        	<div class="title">
                            	<h3>Popular Posts</h3>
                            </div><!-- end title -->
                               <ul class="recent_posts_widget">
                                   <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '3',
                 'post_type' => 'post',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'thum');
			if($thumb_url[0]==""){
				$get_img = "http://www.uprealproperty.com/no-photo.jpg";
			}
			
			else{
				$get_img = $thumb_url[0];
			}
         ?>

								  <li>
                                        <a href="<?php the_permalink();?>">
                                        <img width="50px" height="50px;" src="<?php echo $get_img;?>" alt="" /> <?php if(mb_strlen($post->post_title)>45) { $cont= mb_substr($post->post_title,0,45) ; echo strip_tags($cont. ･･･ );} else {echo strip_tags($post->post_title);}?>
                                        </a>
                                        <a class="readmore" href="<?php the_permalink();?>"><?php the_time('l d, Y') ?></a>
                                    </li>
        <?php endwhile;endif;?>
 
                                </ul><!-- recent posts --> 
                        </div><!-- end widget -->
                    </div><!-- end col-lg-3 -->
          			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					                              <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-right-widget') ) ?>

                    
                    </div><!-- end col-lg-3 -->
            	</div><!-- end row -->

            </div><!-- end container -->
											<div class="copy_right">© Copyright 2014 - UP Real Estate & Property Services. All rights reserved.</div>

        </footer><!-- end footer --> 

    </div>    <div class="dmtop">
    	Scroll to Top    </div><!-- end dmtop -->


	<!-- Bootstrap core and JavaScript's
    ================================================== -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.parallax.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fitvids.js"></script>    
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.unveilEffects.js"></script>	
	<script src="<?php echo get_template_directory_uri(); ?>/js/retina-1.1.0.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/fhmm.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/application.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/smoothscroll.js"></script>


	<!-- Skills JavaScript
    ================================================== -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.animate-enhanced.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easypiechart.min.js"></script>
	<script type="text/javascript">
	$('.chart').easyPieChart({
		easing: 'easeOutBounce',
		size : 200,
		animate : 2000,
		lineWidth : 2,
		lineCap : 'square',
		lineWidth : 3,
		barColor : false,
		trackColor : '#F5F5F5',
		scaleColor : false,
		onStep: function(from, to, percent) {
		$(this.el).find('.percent').text(Math.round(percent)+'%');
		}
	});
	</script>

	<!-- FlexSlider JavaScript
    ================================================== -->
 	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
	<script>
        $(window).load(function() {
            $('#aboutslider').flexslider({
                animation: "fade",
                controlNav: true,
                animationLoop: false,
                slideshow: true,
                sync: "#carousel"
            });
        });
    </script>
     
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.isotope.min.js"></script>
	<script type="text/javascript">
	// Portfolio
	(function($) {
		"use strict";
		var $container = $('.portfolio_wrapper'),
			$items = $container.find('.item'),
			portfolioLayout = 'masonry';
			
			$container.isotope({
				filter: '*',
				animationEngine: 'best-available',
				layoutMode: portfolioLayout,
				animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			},
			masonry: {
			}
			}, refreshWaypoints());
			
			function refreshWaypoints() {
				setTimeout(function() {
				}, 1000);   
			}
					
			$('nav.portfolio-filter ul a').on('click', function() {
					var selector = $(this).attr('data-filter');
					$container.isotope({ filter: selector }, refreshWaypoints());
					$('nav.portfolio-filter ul a').removeClass('active');
					$(this).addClass('active');
					return false;
			});
			
			function getColumnNumber() { 
				var winWidth = $(window).width(), 
				columnNumber = 1;
			
				if (winWidth > 1200) {
					columnNumber = 4;
				} else if (winWidth > 950) {
					columnNumber = 3;
				} else if (winWidth > 600) {
					columnNumber = 2;
				} else if (winWidth > 400) {
					columnNumber = 2;
				} else if (winWidth > 250) {
					columnNumber = 1;
				}
					return columnNumber;
				}       
				
				function setColumns() {
					var winWidth = $(window).width(), 
					columnNumber = getColumnNumber(), 
					itemWidth = Math.floor(winWidth / columnNumber);
					
					$container.find('.item').each(function() { 
						$(this).css( { 
						width : itemWidth + 'px'
					});
				});
			}
			
			function setPortfolio() { 
				setColumns();
				$container.isotope('reLayout');
			}
				
			$container.imagesLoaded(function () { 
				setPortfolio();
			});
			
			$(window).on('resize', function () { 
			setPortfolio();          
			});
	
		})(jQuery);
	</script>	
</body>
</html>