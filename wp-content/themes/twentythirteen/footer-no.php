
 <footer class="footer1 jt-shadow clearfix">
        	<div class="container">
            	<div class="row">
          			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    	                    	            <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-left-widget') ) ?>

                    </div><!-- end col-lg-3 -->
          			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    	<div class="widget clearfix">
                        	<div class="title">
                            	<h3>Contact Info</h3>
                            </div><!-- end title -->
                            <div class="contact-widget">
                              <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-center-widget') ) ?>

                                <ul class="social clearfix">
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook" title="" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Google Plus" title="" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter" title="" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Youtube" title="" href="#"><i class="fa fa-youtube"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Linkedin" title="" href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Dribbble" title="" href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Skype" title="" href="#"><i class="fa fa-skype"></i></a></li>
                                </ul><!-- end social -->
                            </div><!-- contact-widget -->
                        </div><!-- end widget -->
                    </div><!-- end col-lg-3 -->
          			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    	 <div class="widget clearfix">
                        	<div class="title">
                            	<h3>Popular Posts</h3>
                            </div><!-- end title -->
                               <ul class="recent_posts_widget">
                                   <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '3',
                 'post_type' => 'post',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'thum');
		if($thumb_url[0]==""){
				$get_img = "http://www.uprealproperty.com/no-photo.jpg";
			}
			
			else{
				$get_img = $thumb_url[0];
			}
         ?>

								  <li>
                                        <a href="<?php the_permalink();?>">
                                        <img width="50px" height="50px;" src="<?php echo $get_img;?>" alt="" /> <?php if(mb_strlen($post->post_title)>45) { $cont= mb_substr($post->post_title,0,45) ; echo strip_tags($cont. ･･･ );} else {echo strip_tags($post->post_title);}?>
                                        </a>
                                        <a class="readmore" href="<?php the_permalink();?>"><?php the_time('l d, Y') ?></a>
                                    </li>
        <?php endwhile;endif;?>
 
                                </ul><!-- recent posts --> 
                        </div><!-- end widget -->
                    </div><!-- end col-lg-3 -->
          			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					                              <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-right-widget') ) ?>

                    	<!--<div class="widget clearfix">
                        	<div class="title">
                            	<h3>Drop us a line!</h3>
                            </div><!-- end title 
                        	<div class="footer-form">
                            <form id="contact" action="" name="contactform" method="post">
                            	<input type="text" name="name" id="name" class="form-control" placeholder="Name"> 
                            	<input type="text" name="email" id="email" class="form-control" placeholder="Email Address"> 
                            	 <textarea class="form-control" name="message" id="message" rows="4" placeholder="Message"></textarea>
                                 <div class="text-center">
                                 <button type="submit" value="SEND" id="submit" class="btn btn-primary">SEND</button>
                                 </div>
							</form>                            
                            </div>end footer-form
                        </div>end widget -->
                    </div><!-- end col-lg-3 -->
            	</div><!-- end row -->
            </div><!-- end container -->
        </footer><!-- end footer --> 

    </div>    <div class="dmtop">
    	Scroll to Top    </div><!-- end dmtop -->


	<!-- Bootstrap core and JavaScript's
    ================================================== -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.parallax.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fitvids.js"></script>    
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.unveilEffects.js"></script>	
	<script src="<?php echo get_template_directory_uri(); ?>/js/retina-1.1.0.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/fhmm.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/application.js"></script>


	<!-- Skills JavaScript
    ================================================== -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.animate-enhanced.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easypiechart.min.js"></script>
	<script type="text/javascript">
	$('.chart').easyPieChart({
		easing: 'easeOutBounce',
		size : 200,
		animate : 2000,
		lineWidth : 2,
		lineCap : 'square',
		lineWidth : 3,
		barColor : false,
		trackColor : '#F5F5F5',
		scaleColor : false,
		onStep: function(from, to, percent) {
		$(this.el).find('.percent').text(Math.round(percent)+'%');
		}
	});
	</script>

	<!-- FlexSlider JavaScript
    ================================================== -->
 	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
	<script>
        $(window).load(function() {
            $('#aboutslider').flexslider({
                animation: "fade",
                controlNav: true,
                animationLoop: false,
                slideshow: true,
                sync: "#carousel"
            });
        });
    </script>
     
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.isotope.min.js"></script>
	<script type="text/javascript">
	// Portfolio
	(function($) {
		"use strict";
		var $container = $('.portfolio_wrapper'),
			$items = $container.find('.item'),
			portfolioLayout = 'masonry';
			
			$container.isotope({
				filter: '*',
				animationEngine: 'best-available',
				layoutMode: portfolioLayout,
				animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			},
			masonry: {
			}
			}, refreshWaypoints());
			
			function refreshWaypoints() {
				setTimeout(function() {
				}, 1000);   
			}
					
			$('nav.portfolio-filter ul a').on('click', function() {
					var selector = $(this).attr('data-filter');
					$container.isotope({ filter: selector }, refreshWaypoints());
					$('nav.portfolio-filter ul a').removeClass('active');
					$(this).addClass('active');
					return false;
			});
			
			function getColumnNumber() { 
				var winWidth = $(window).width(), 
				columnNumber = 1;
			
				if (winWidth > 1200) {
					columnNumber = 4;
				} else if (winWidth > 950) {
					columnNumber = 3;
				} else if (winWidth > 600) {
					columnNumber = 2;
				} else if (winWidth > 400) {
					columnNumber = 2;
				} else if (winWidth > 250) {
					columnNumber = 1;
				}
					return columnNumber;
				}       
				
				function setColumns() {
					var winWidth = $(window).width(), 
					columnNumber = getColumnNumber(), 
					itemWidth = Math.floor(winWidth / columnNumber);
					
					$container.find('.item').each(function() { 
						$(this).css( { 
						width : itemWidth + 'px'
					});
				});
			}
			
			function setPortfolio() { 
				setColumns();
				$container.isotope('reLayout');
			}
				
			$container.imagesLoaded(function () { 
				setPortfolio();
			});
			
			$(window).on('resize', function () { 
			setPortfolio();          
			});
	
		})(jQuery);
	</script>	
	<style type="text/css">

.acf-map {
	width: 100%;
	height: 600px;
	border: #ccc solid 1px;
	margin: 20px 0;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
(function($) {

/*
*  render_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function render_map( $el ) {

	// var
	var $markers = $el.find('.marker');

	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};

	// create map	        	
	var map = new google.maps.Map( $el[0], args);

	// add a markers reference
	map.markers = [];

	// add markers
	$markers.each(function(){

    	add_marker( $(this), map );

	});

	// center map
	center_map( map );

}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/

$(document).ready(function(){

	$('.acf-map').each(function(){

		render_map( $(this) );

	});

});

})(jQuery);
</script>
</body>
</html>