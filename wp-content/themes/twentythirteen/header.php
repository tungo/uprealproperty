<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
 <title><?php wp_title(); ?></title>

	<!-- Bootstrap core CSS -->
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" rel="stylesheet">
	<!-- Style CSS -->
	<link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900' rel='stylesheet' type='text/css'>

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/rs-plugin/css/settings.css" media="screen" />
    
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5shiv.js"></script>
	  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/respond.min.js"></script>
	<![endif]-->

	<!-- Favicons -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/ico/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="assets/ico/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="assets/ico/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/ico/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/ico/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/ico/apple-touch-icon-144x144.png">
    
</head>
<body data-spy="scroll" data-offset="75">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54497812-1', 'auto');
  ga('send', 'pageview');

</script>
<div class="animationload"><div id="intro"></div></div>

	<div class="wrapper">
    	<section class="topbar clearfix">
        	<div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<div class="social clearfix">
                            <ul>
                                <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook" title="" href="https://www.facebook.com/pages/Thomas-Nguyen-realtor/427571967302021"><i class="fa fa-facebook"></i></a></li>
                                <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Google Plus" title="" href="https://plus.google.com/u/0/+ThomasNguyenbayarearealestates/posts"><i class="fa fa-google-plus"></i></a></li>
                                <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter" title="" href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Youtube" title="" href="http://www.youtube.com/channel/UC5swR1GaMzL57rF59O0D2ug"><i class="fa fa-youtube"></i></a></li>
                                <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Linkedin" title="" href="http://www.linkedin.com/profile/view?id=168680753&trk=tab_pro"><i class="fa fa-linkedin"></i></a></li>
                              
                                <li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Skype" title="" href="#"><i class="fa fa-skype"></i></a></li>
                            </ul>
                        </div><!-- end social -->
                    </div><!-- end col-lg-6 -->
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<div class="callus clearfix">
                        	<ul>
                                <li><p><span>Email :</span> Thomas.nguyen@uprealproperty.com</p></li>
                                <li><p><span>Phone :</span> (415) 361-8535</p></li>
                              
                            </ul>
                        </div><!-- end callus -->
                    </div><!-- end col-lg-6 -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end topbar -->

 		<header class="header clearfix">
			<div class="container">
            	<div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                       <div class="logo-wrapper clearfix">
							<div class="logo">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Home">
									<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Estate">
								</a>
							</div><!-- /.site-name -->
						</div><!-- /.logo-wrapper -->
                    </div><!-- end col-lg-4 -->
                    <div class="col-lg-8 col-md-8 col-sm-12">
						<nav class="navbar navbar-default fhmm" role="navigation">
                            <div class="navbar-header">
                                <button type="button" data-toggle="collapse" data-target="#defaultmenu" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                            </div><!-- end navbar-header -->
                            <div id="defaultmenu" class="navbar-collapse collapse container">
						
								
													<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav navbar-nav' ) ); ?>

                           		<ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <div id="dmsearch" class="dmsearch">
											<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                                                <div class="dm-search-container">
                                                <input id="s" class="dmsearch-input" type="text" name="s" value="" placeholder="Search on this site">
                                                </div>
                                                <input id="go" class="dmsearch-submit" type="submit" value="">
                                                <span class="searchicon"></span>
                                            </form> <!-- end searchform -->
                                        </div><!-- end search -->
                                    </li>
                           		</ul><!-- end nav navbar-nav navbar-right -->                                    
                            </div><!-- end #navbar-collapse-1 -->
						</nav><!-- end navbar navbar-default fhmm -->
                    </div><!-- end col-lg-8 -->  
				</div><!-- end row -->
			</div><!-- end container -->
		</header><!-- end header -->	           
