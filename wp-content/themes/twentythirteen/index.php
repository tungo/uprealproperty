<?php 
get_header(); 
?>
        <section class="sliderwrapper clearfix">
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>
               		<!-- SLIDE  -->
                       
                   		<!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo get_template_directory_uri(); ?>/slider-images/banner1.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                   
                        </li>
                    	<!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo get_template_directory_uri(); ?>/slider-images/02_bg.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption customin customout"
                                data-x="left" data-hoffset="0"
                                data-y="bottom" data-voffset="0"
                                data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="800"
                                data-start="700"
                                data-easing="Power4.easeOut"
                                data-endspeed="500"
                                data-endeasing="Power4.easeIn"
                                style="z-index: 3"><img src="<?php echo get_template_directory_uri(); ?>/slider-images/01_man.png" alt="">
                            </div>
                         
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption nothing1 skewfromright customout"
                                data-x="left" data-hoffset="300"
                                data-y="265"
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="800"
                                data-start="1700"
                                data-easing="Power4.easeOut"
                                data-endspeed="300"
                                data-endeasing="Power1.easeIn"
                                data-captionhidden="on"
                                style="z-index: 7">Thomas Nguyen
                            </div>
			    
			      
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption nothing1 colorblue skewfromright customout"
                                data-x="right" data-hoffset="-150"
                                data-y="415"
                                data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                data-speed="1200"
                                data-start="2200"
                                data-easing="Power4.easeOut"
                                data-endspeed="300"
                                data-endeasing="Power1.easeIn"
                                data-captionhidden="on"
                                style="z-index: 7"><a href="http://www.leading-sf.com/contact/"> Call:  (415) 361-8535</a>
                            </div>
        
                        </li>
                    			<!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo get_template_directory_uri(); ?>/	slider-images/banner2.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                   
                        </li>
						<!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo get_template_directory_uri(); ?>/slider-images/banner3.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                   
                        </li>
						<!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo get_template_directory_uri(); ?>/slider-images/banner4.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                   
                        </li>
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
			</div>       
        </section><!-- end slider-wrapper -->  
            <style>
			.skewfromleft{
			background:#000000;
			opacity: 0.6 !important;
			line-height: 45px !important;
			color:#ffffff !important;
			}
			
			</style>
        <section class="whitewrapper bordertop clearfix">
        	<div class="container">
            	<div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-box service-icon text-center">
                            <div class="icn-main-container">
                                <div class="icn-container" data-effect="slide-bottom">
                                    <div class="serviceicon"><i class="fa fa-lightbulb-o"></i></div>
                                </div>
                            </div> 
                            <div class="title"><h3>Retail Sites for Lease</h3></div>
                            <p>Fusce at ornare massa. Donec et vehicula nunc. Nulla ut venenat gula. Vestibulum hendrerit diam nunc, in tempus urna rhoncus faucibus.</p>
                        </div>
                    </div><!-- end col-lg-4 -->
                                            
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-box service-icon text-center">
                            <div class="icn-main-container">
                                <div class="icn-container" data-effect="slide-bottom">
                                    <div class="serviceicon"><i class="fa fa-share-square-o"></i></div>
                                </div>
                            </div> 
                            <div class="title"><h3>Office Sites for Lease</h3></div>
                            <p>Fusce at ornare massa. Donec et vehicula nunc. Nulla ut venenat gula. Vestibulum hendrerit diam nunc, in tempus urna rhoncus faucibus.</p>
                        </div>
                    </div><!-- end col-lg-4 -->
                                            
                   <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-box service-icon text-center">
                            <div class="icn-main-container">
                                <div class="icn-container" data-effect="slide-bottom">
                                    <div class="serviceicon"><i class="fa fa-headphones"></i></div>
                                </div>
                            </div> 
                            <div class="title"><h3>Investment Properties</h3></div>
                            <p>Fusce at ornare massa. Donec et vehicula nunc. Nulla ut venenat gula. Vestibulum hendrerit diam nunc, in tempus urna rhoncus faucibus.</p>
                        </div>
                    </div><!-- end col-lg-4 -->                         
            	</div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end whitewrapper -->
    


		<section id="two-parallax" class="parallax" style="background-image: url('<?php echo get_template_directory_uri(); ?>/demos/04_parallax.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
        <div class="overlay">
        	<div class="container">
            	<div class="row">
                	<div class="fullwidth paddingtop clearfix">
                    

                    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        	<div class="title"><h1>WELCOME TO UP Real Estate & Property Serives</h1></div>
							<p>UP Real Estate & Property Services is a San Francisco based company specializing in the sales and leasing of properties for both old & new immigrants reside San Francisco Bay Area.  We represent buyers, sellers, tenants, landlords and foreign investors in San Francisco Bay Area.  We have a unique customized market analysis, investment planning that best suited to client's needs & wants.</p>
                         	<div class="clearfix"></div>
                            
                           <!-- <div class="about_icons text-center">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="service-icon">
                                        <div class="dm-icon-effect-1" data-effect="slide-bottom">
                                            <a href="#" class=""> <i class="hovicon effect-1 sub-a fa fa-lightbulb-o fa-2x"></i> </a>
                                        </div>
                                        <div class="title"><h3>Professional Experience</h3></div>
                                    </div>
                                </div>
    
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="service-icon">
                                        <div class="dm-icon-effect-1" data-effect="slide-bottom">
                                            <a href="#" class=""> <i class="hovicon effect-1 sub-a fa fa-share-square-o fa-2x"></i> </a>
                                        </div>
                                        <div class="title"><h3>Awards and Designations</h3></div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="service-icon">
                                        <div class="dm-icon-effect-1" data-effect="slide-bottom">
                                            <a href="#" class=""> <i class="hovicon effect-1 sub-a fa fa-headphones fa-2x"></i> </a>
                                        </div>
                                        <div class="title"><h3>Education and Credentials</h3></div>
                                    </div>
                                </div>
                            </div> about_icons -->
                        </div><!-- end col-lg-6 -->
                	</div><!-- end fullwidth -->
                </div><!-- end row -->
            </div><!-- end container -->
            </div>
        </section><!-- end darkwrapper -->

		
        <section class="whitewrapper jt-shadow clearfix">
        	<div class="container">
            	<div class="row text-center">
        			<div class="title">
                        <h1>OUR TEAM</h1>
                        <hr>
                        <p class="lead">We bring you an awesomeness ...</p>
                    </div>
                      <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '4',
                 'post_type' => 'team',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'full');
			
         ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    	<div class="teambox clearfix first" data-effect="slide-bottom">
                            <div class="media_element ImageWrapper boxes_img">
                                <a href="#"><img src="<?php echo $thumb_url[0];?>" alt=""></a>
                                <div class="ImageOverlayH"></div>
                                <div class="Buttons StyleSc">
                                    <span class="WhiteRounded"><a data-placement="bottom" data-toggle="tooltip" data-original-title="Follow on Facebook" title="" href="<?php echo get_field('facebook');?>"><i class="fa fa-facebook"></i></a></span>
                                    <span class="WhiteRounded"><a data-placement="bottom" data-toggle="tooltip" data-original-title="Follow on Twitter" title="" href="<?php echo get_field('twitter');?>"><i class="fa fa-twitter"></i></a></span>
                                    <span class="WhiteRounded"><a data-placement="bottom" data-toggle="tooltip" data-original-title="Follow on Google Plus" title="" href="<?php echo get_field('google');?>"><i class="fa fa-google-plus"></i></a></span>
                                </div><!-- Buttons -->
                            </div><!-- media_element -->
                            <div class="title">
                            	<h3><?php the_title();?></h3>
                                <p class="lead"> <?php echo get_field('position');?></p>
                            </div><!-- end title -->
                            <p>  <?php echo get_field('description');?></p>
                        </div><!-- end teambox -->
                    </div><!-- col-lg-3 -->
                        <?php endwhile;endif;?>

                    
            	</div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end whitewrapper -->
		       <section class="whitewrapper greybg jt-shadow bordertop clearfix">
        	<div class="container">
            	<div class="row">
        			<div class="title text-center">
        <h1>Testimonials</h1>
        <p class="lead">We help you navigate the ever-changing landscape of California property laws. </p>
        <hr>
      </div>
      <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '2',
                 'post_type' => 'chungthuc',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'230x230');
			
         ?>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="testimonial_wrap">
          <div class="testimonial_details" data-effect="slide-left"> <img class="img-circle alignleft" src="<?php echo $thumb_url[0];?>" alt="">
            <p><?php echo get_field('content');?></p>
            <div class="testimonial_meta clearfix">
              <div class="pull-left">
                <p><span>
                  <?php the_title();?>
                  </span> - <?php echo get_field('position');?></p>
              </div>
              <div class="pull-right">
                <div class="rating text-center"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                <!-- rating --> 
              </div>
            </div>
            <!-- testimonial_desc --> 
          </div>
          <!-- end testimonial_meta --> 
        </div>
        <!-- end testimonial_wrap --> 
      </div>
      <?php endwhile;endif;?>
    

     

    		
     <?php get_footer(); ?>