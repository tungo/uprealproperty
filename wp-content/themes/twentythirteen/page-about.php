<?php 
/*
Template Name: About
*/
get_header('inpage'); 
?>

<section class="post-wrapper-top dm-shadow clearfix">
  <div class="container">
    <div class="col-lg-12">
      <h2>About me</h2>
      <ul class="breadcrumb pull-right">
        <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
        <li>About me</li>
      </ul>
    </div>
  </div>
</section>
<!-- end post-wrapper-top -->

<section class="postwrapper clearfix">
  <div class="container">
    <div class="row">
      <div class="fullwidth clearfix">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div id="aboutslider" class="flexslider clearfix">
            <ul class="slides">
              <li><img src="<?php echo get_template_directory_uri(); ?>/images/about1.jpg" alt=""></li>
              <li><img src="<?php echo get_template_directory_uri(); ?>/images/about2.jpg" alt=""></li>
              <li><img src="<?php echo get_template_directory_uri(); ?>/images/about3.jpg" alt=""></li>
            </ul>
            <!-- end slides -->
            <div class="aboutslider-shadow"> <span class="s1"></span> </div>
          </div>
          <!-- end slider --> 
        </div>
        <!-- end col-lg-6 -->
        
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="title">
            <h1>Personal Background</h1>
          </div>
          <p>In 2001, I made an excellent decision by joining the Century 21 team. Since then, I have had great success representing both buyers and sellers in San Francisco and the Bay Area. Over the past 13 years, I have gained invaluable experience that has not only made me an accomplished realtor, but has also allowed me to provide each and every one of my clients with exemplary service. As a Century 21 agent, I embrace the motto of “Smarter, Faster, and Bolder”, and I look forward to assisting many more buyers and sellers with all of their real estate needs.</p>
          <div class="clearfix"></div>
          <div class="about_icons text-center">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <div class="service-icon">
                <div class="dm-icon-effect-1" data-effect="slide-bottom"> <a href="#" class="experience"> <i class="hovicon effect-1 sub-a fa fa-lightbulb-o fa-2x"></i> </a> </div>
                <div class="title">
                  <h3>Experience</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <div class="service-icon">
                <div class="dm-icon-effect-1" data-effect="slide-bottom"> <a href="#" class="awards"> <i class="hovicon effect-1 sub-a fa fa-star fa-2x"></i> </a> </div>
                <div class="title">
                  <h3>Awards</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <div class="service-icon">
                <div class="dm-icon-effect-1" data-effect="slide-bottom"> <a href="#" class="credentials"> <i class="hovicon effect-1 sub-a fa fa-list fa-2x"></i> </a> </div>
                <div class="title">
                  <h3>Credentials</h3>
                </div>
              </div>
            </div>
          </div>
          <!-- about_icons --> 
        </div>
        <!-- end col-lg-6 --> 
      </div>
      <!-- end fullwidth --> 
    </div>
    <!-- end row --> 
  </div>
  <!-- end container --> 
</section>
<!-- end postwrapper -->

<section id="one-parallax" class="parallax" style="background-image: url('<?php echo get_template_directory_uri(); ?>/demos/04_parallax.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="fullwidth paddingtop clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="title">
              <h1>“Above and Beyond Excellence is My Standard!”</h1>
            </div>
            <p>When it comes to real estate services, I continually strive to create a “WOW” experience that exceeds all of my client’s expectations. I willingly work overtime in order to find my clients their dream home- my clients know that they can count on me to work hard and go the extra mile to get the job done in any circumstances.</p>
            <p> With over 20 years of experience
              in business sales negotiating, I approach my clients’ needs with integrity and a sense of professionalism in order smoothly complete each real estate transaction. Complete client satisfaction is my main goal, and my commitment to this goal is clearly outlined in a 21 point “Buyer-Seller Service Pledge”, which I provide to each person that I work with.</p>
            <p>I understand that buying or selling a home is a huge decision, which is why I thoroughly research everything before advising my clients. The real estate market is always changing, but I spend a lot of time to ensure that I am up to date on the latest trends and newest developments- my clients can count on me for a deep knowledge of the industry, as well as the current market conditions. In short, I do everything possible to make the home buying process SIMPLE and FUN for my clients.
              My background and experience have given me the skills needed to be an excellent realtor; I am a tough, solid negotiator, flexible, a quick and creative problem solver, and entirely adaptable to any situation. In addition, I excel in working with clients of different ethnic backgrounds or cultures.</p>
            <p> Century 21 is the number one real estate company in the world, and I am proud to be a member of this franchise. I currently work with a team of over 70 talented individuals, and together we create a strong and effective sales force that cannot be matched by other real estate firms. When you work with an agent from Century 21, you can count on the backing of a real estate company with a reach into over 70 countries, with more than 120,000 real estate professionals working around the globe. In addition to all of the benefits that being a Century 21 agent provides me with, I am also well networked with a large number of real estate agents and REALTORS, both locally and internationally.
              Over the past 8 years, I have been fortunate to establish a strong buyer’s representation for foreigners. In particular, I specialize in assisting buyers and investors from China, and Pacific Rim countries, such as Cambodia, and Vietnam. With this connection to a large source of buyers, I am able to better serve my clients who want to sell their properties.</p>
            <div class="clearfix"></div>
          </div>
          <!-- end col-lg-6 --> 
        </div>
        <!-- end fullwidth --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container --> 
  </div>
  <div id="experience"></div>
</section>
<!-- end darkwrapper -->

<section class="about_width">
  <div class="col-lg-6 col-md-6 col-sm-12 bigimg">
    <div data-effect="slide-left"> <img width="100%" src="<?php echo get_template_directory_uri(); ?>/demos/01_blog.png" alt=""> </div>
    <!-- image-container --> 
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12">
    <div class="title1">
      <h1 id="experience_down">Professional Experience</h1>
      <hr>
    </div>
    <ul>
      <li>More than 13 years of experience in the San Francisco and Bay Area real estate market</li>
      <li>2013 Board Director & Officer for Chinese Real Estate Association of America</li>
      <li>Member of San Francisco Association of Realtors</li>
      <li>Member of National Association of Realtors</li>
      <li>Member of California Association of Realtors</li>
      <li>Member of  Institute of Luxury Home Marketing</li>
      <li>Member of Asian Real Estate  of America</li>
    </ul>
  </div>
  <div class="clearfix"></div>
  <hr>
  <div class="col-lg-6 col-md-6 col-sm-12 css_right">
    <div class="title1">
      <h1 id="awards">Awards and Designations</h1>
      <hr>
    </div>
    <ul>
      <li>Top 1% Agent in my office</li>
      <li>Centurion Top 5% producer since 2003</li>
      <li>Grand Centurion Team top 1% since 2012</li>
      <li>Grand Centurion Team top 1% in 2013</li>
      <li>Grand Centurion production- in the top1% nationwide</li>
      <li>Together with my team, we sold 161 properties in the San Francisco/ Bay Area in 2012, making us the top ranking real estate team in Northern California, and 3rd ranking in the world</li>
    </ul>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12">
    <div data-effect="slide-right"> <img width="100%" src="<?php echo get_template_directory_uri(); ?>/demos/04_blog.jpg" alt=""> </div>
    <!-- image-container --> 
  </div>
  <div class="clearfix"></div>
  <hr>
  <div class="col-lg-6 col-md-6 col-sm-12">
    <div data-effect="slide-bottom"> <img width="100%" src="<?php echo get_template_directory_uri(); ?>/demos/03_blog.png" alt=""> </div>
    <!-- image-container --> 
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12">
    <div class="title1">
      <h1 id="credentials">Education and Credentials</h1>
      <hr>
    </div>
    <ul>
      <li>Associates Degree- Business Finance</li>
      <li>Bachelor’s of Science- Business Administration</li>
      <li>Certified Residential Specialist (CRS)</li>
      <li>Certified Short Sale and Foreclosure Resource (SFR)</li>
      <li>Certified HAFA Specialist (CHS)</li>
      <li>Certified Social Media and E-Marketing Specialist</li>
      <li>Certified Selling Apartment Buildings</li>
      <li>Certified Commercial Specialist</li>
      <li>Broker Licensed by California Bureau of Real Estate</li>
	  <li>Property Management Certification "PMC" by C.A.R​</li>
    </ul>
  </div>
  <div class="clearfix"></div>
  <hr>
</section>
<section class="whitewrapper bordertop clearfix">
  <div class="container">
    <div class="row">
      <div class="title text-center">
        <h1>CLIENTS FEEDBACK</h1>
        <p class="lead">We help you navigate the ever-changing landscape of California property laws. </p>
        <hr>
      </div>
      <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '10',
                 'post_type' => 'chungthuc',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID);
			
         ?>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="testimonial_wrap">
          <div class="testimonial_details" data-effect="slide-left"> <img class="img-circle alignleft" src="<?php echo $thumb_url[0];?>" alt="">
            <p><?php echo get_field('content');?></p>
            <div class="testimonial_meta clearfix">
              <div class="pull-left">
                <p><span>
                  <?php the_title();?>
                  </span> - <?php echo get_field('position');?></p>
              </div>
              <div class="pull-right">
                <div class="rating text-center"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                <!-- rating --> 
              </div>
            </div>
            <!-- testimonial_desc --> 
          </div>
          <!-- end testimonial_meta --> 
        </div>
        <!-- end testimonial_wrap --> 
      </div>
      <?php endwhile;endif;?>
<?php get_footer('inpage'); ?>