<?php 
/*
Template Name: Contact
*/
get_header(); 
?>
           

		<section class="map clearfix">
			<div id="map"></div>
		</section><!-- end map -->
            
		<section class="postwrapper clearfix">
        	<div class="container">
            	<div class="row">
                	<div class="fullwidth clearfix">
                    	<div class="contact_details">
                            <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                                <div class="miniboxes text-center" data-effect="helix">
                                    <div class="miniicon"><i class="fa fa-map-marker fa-2x"></i></div>
                                    <div class="title"><h3>Address</h3></div>
                                    <p> 350 Rhode Island Street, Suite 240,<br/> San Francisco, CA 94103</p>
                                </div>
                            </div><!-- end col-lg-4 -->
                            <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                                <div class="miniboxes text-center" data-effect="helix">
                                    <div class="miniicon"><i class="fa fa-mobile-phone fa-2x"></i></div>
                                    <div class="title"><h3>Mobile Number</h3></div>
                                    <p><strong>Phone:</strong>(415) 361-8535 <br>
                                    <strong>Fax:</strong>(800) 538-6489 </p>
                                </div>
                            </div><!-- end col-lg-4 -->
                            <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                                <div class="miniboxes text-center" data-effect="helix">
                                    <div class="miniicon"><i class="fa fa-headphones fa-2x"></i></div>
                                    <div class="title"><h3>Email</h3></div>
                                    <p>Thomas.nguyen@Uprealproperty.com<br>
                                   
                                    </p>
                                </div>
                            </div><!-- end col-lg-4 -->
                        </div><!-- end contact_details -->
                        
                        <div class="clearfix"></div>
                        
                        
                               
                	</div><!-- end fullwidth -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end postwrapper -->

        <section class="whitewrapper bordertop clearfix">
        	<div class="container">
            	<div class="row">
        		<div class="title text-center">
        <h1>CLIENTS FEEDBACK</h1>
        <p class="lead">We help you navigate the ever-changing landscape of California property laws. </p>
        <hr>
      </div>
      <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '2',
                 'post_type' => 'chungthuc',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'230x230');
			
         ?>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="testimonial_wrap">
          <div class="testimonial_details" data-effect="slide-left"> <img class="img-circle alignleft" src="<?php echo $thumb_url[0];?>" alt="">
            <p><?php echo get_field('content');?></p>
            <div class="testimonial_meta clearfix">
              <div class="pull-left">
                <p><span>
                  <?php the_title();?>
                  </span> - <?php echo get_field('position');?></p>
              </div>
              <div class="pull-right">
                <div class="rating text-center"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                <!-- rating --> 
              </div>
            </div>
            <!-- testimonial_desc --> 
          </div>
          <!-- end testimonial_meta --> 
        </div>
        <!-- end testimonial_wrap --> 
      </div>
      <?php endwhile;endif;?>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">
 
		var locations = [
		['<div class="infobox"><img class="alignleft img-responsive" src="<?php echo get_template_directory_uri(); ?>/demos/01_office.jpg" alt=""><h3 class="title"><a href="about1.html">OUR USA OFFICE</a></h3><span>San Francisco, CA 94103</span><br>415-361-8535</p></div></div></div>', 37.765671,-122.40268, 17]
		];
	
		var map = new google.maps.Map(document.getElementById('map'), {
		  zoom:16,
			scrollwheel: false,
			navigationControl: true,
			mapTypeControl: true,
			scaleControl: false,
			draggable: true,
			styles: [ { "stylers": [ { "hue": "red" }, { "gamma": 1 } ] } ],
			center: new google.maps.LatLng(37.765671,-122.40268),
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		});
	
		var infowindow = new google.maps.InfoWindow();
	
		var marker, i;
	
		for (i = 0; i < locations.length; i++) {  
	  
			marker = new google.maps.Marker({ 
			position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
			map: map ,
			icon: '<?php echo get_template_directory_uri(); ?>/images/marker.png'
			});
	
	
		  google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
			  infowindow.setContent(locations[i][0]);
			  infowindow.open(map, marker);
			}
		  })(marker, i));
		}
	</script>	


     <?php get_footer(); ?>