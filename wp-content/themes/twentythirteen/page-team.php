<?php 
/*
Template Name: Contact
*/
get_header('inpage'); 
?>
           
  <section class="whitewrapper jt-shadow clearfix">
        	<div class="container">
            	<div class="row text-center">
        			<div class="title">
                        <h1>OUR TEAM</h1>
                        <hr>
                        <p class="lead">We bring you an awesomeness ...</p>
                    </div>
                      <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '4',
                 'post_type' => 'team',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'full');
			
         ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    	<div class="teambox clearfix first" data-effect="slide-bottom">
                            <div class="media_element ImageWrapper boxes_img">
                                <a href="#"><img src="<?php echo $thumb_url[0];?>" alt=""></a>
                                <div class="ImageOverlayH"></div>
                                <div class="Buttons StyleSc">
                                    <span class="WhiteRounded"><a data-placement="bottom" data-toggle="tooltip" data-original-title="Follow on Facebook" title="" href="<?php echo get_field('facebook');?>"><i class="fa fa-facebook"></i></a></span>
                                    <span class="WhiteRounded"><a data-placement="bottom" data-toggle="tooltip" data-original-title="Follow on Twitter" title="" href="<?php echo get_field('twitter');?>"><i class="fa fa-twitter"></i></a></span>
                                    <span class="WhiteRounded"><a data-placement="bottom" data-toggle="tooltip" data-original-title="Follow on Google Plus" title="" href="<?php echo get_field('google');?>"><i class="fa fa-google-plus"></i></a></span>
                                </div><!-- Buttons -->
                            </div><!-- media_element -->
                            <div class="title">
                            	<h3><a target="_blank" href="<?php echo get_field('position');?>"><?php the_title();?></a></h3>
                                <p class="lead"> <?php echo get_field('position');?></p>
                            </div><!-- end title -->
                            <p>  <?php echo get_field('description');?></p>
                        </div><!-- end teambox -->
                    </div><!-- col-lg-3 -->
                        <?php endwhile;endif;?>

                    
            	</div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end whitewrapper -->	


        <section class="whitewrapper bordertop clearfix">
        	<div class="container">
            	<div class="row">
     <?php get_footer('inpage'); ?>