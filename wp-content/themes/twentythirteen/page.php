<?php 
get_header('inpage'); 
?>	           

        <section class="post-wrapper-top dm-shadow clearfix">
            <div class="container">
                <div class="col-lg-12">
                    <h2>	<?php the_title(); ?></h2>
                     <!--<ul class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li>	<?php// the_title(); ?></li>
                    </ul>-->
                </div>
            </div>
        </section><!-- end post-wrapper-top -->
        
		<section class="postwrapper clearfix">
        	<div class="container">
            	<div class="row">
                	<div class="half-width clearfix">
						
                    	<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                
                 <?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					
				</article><!-- #post -->

			
			<?php endwhile; ?>

                                        
				
                            
          				</div><!-- end col-lg-12 -->
                       
						<div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                         	                             

             
                         	<div class="widget">
                            	<div class="title"><h3>NEWS</h3></div>
                                <ul class="recent_posts_widget">
								 <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '7',
                 'post_type' => 'blog',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'thum');
			
         ?>
								
                                    <li>
                                        <a href="<?php the_permalink();?>">
                                        <img src="<?php echo $thumb_url[0];?>" alt="" /> <?php the_title();?>
                                        </a>
                                        <a class="readmore" href="<?php the_permalink();?>"><?php the_time('F j, Y') ?></a>
                                    </li>
                                    <?php endwhile;endif;?>
                                </ul><!-- recent posts -->                              
                            </div><!-- end widget -->

                         	                                                                  
                        </div><!-- end sidebar -->
                	</div><!-- end half-width -->
                </div><!-- end row -->
            </div><!-- end container -->
			
			

        </section><!-- end postwrapper -->

        <?php get_footer('no'); ?>