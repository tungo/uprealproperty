<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
        <section class="post-wrapper-top dm-shadow clearfix">
            <div class="container">
                <div class="col-lg-12">
                    <h2>Blog One Column</h2>
                     <ul class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li>Blog One Column</li>
                    </ul>
                </div>
            </div>
        </section><!-- end post-wrapper-top -->
        
		<section class="postwrapper clearfix">
        	<div class="container">
            	<div class="row">
                	<div class="half-width clearfix">
                    	<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                             <?php if ( have_posts() ) : ?>   
							 <?php while ( have_posts() ) : the_post(); ?>
				           <div class="blog_wrap">
                                <div class="title">
                                    <div class="post_date">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                    <h3><a href="single-blog-sidebar.html" title=""><?php the_title(); ?></a></h3>
                                    <div class="post_meta">
                                        <span><i class="fa fa-calendar"></i>  <?php the_time('l, F jS, Y') ?></span>
                                       
                                    </div><!-- end post-meta -->
                                </div><!-- end title -->
                                <div class="media_element">
                                   
                                </div><!-- media_element -->
                                <div class="post_desc">
                                   		<?php the_excerpt(); ?>

                                    <a class="btn btn-primary" href="<?php the_permalink(); ?>">Read more</a>
                                </div>
                            </div><!-- end blog-wrap -->
			<?php endwhile; ?>
                 
                                             
                         
<?php else : ?>
s		

<?php endif; ?>
				
                                  
                    
                           

							<div class="clearfix"></div>
							<hr>
                            
                           
                           
          				</div><!-- end col-lg-12 -->
                        
						<div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                         	
                         	<div class="widget">
                            	<div class="title"><h3>NEWS</h3></div>
                                <ul class="recent_posts_widget">
								 <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '7',
                 'post_type' => 'blog',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'thum');
			
         ?>
								
                                    <li>
                                        <a href="<?php the_permalink();?>">
                                        <img src="<?php echo $thumb_url[0];?>" alt="" /> <?php the_title();?>
                                        </a>
                                        <a class="readmore" href="<?php the_permalink();?>"><?php the_time('F j, Y') ?></a>
                                    </li>
                                    <?php endwhile;endif;?>
                                </ul><!-- recent posts -->                              
                            </div><!-- end widget -->
                            
                       
                            
                         


                         	                                                                          
                        </div><!-- end sidebar -->
                	</div><!-- end half-width -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end postwrapper -->

<?php get_footer('no'); ?>