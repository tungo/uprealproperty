<?php 
get_header('inpage'); 
?>
<?php if(is_single()): if (have_posts()) : while (have_posts()) : the_post(); ?>

<section class="post-wrapper-top dm-shadow clearfix">
	<div class="container">
		<div class="col-lg-12">
			<h2><?php echo custom_taxonomies_terms_links(); ?></h2>
			<!--<ul class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li>	<?php// the_title(); ?></li>
                    </ul>--> 
		</div>
	</div>
</section>
<!-- end post-wrapper-top -->

<section class="postwrapper clearfix">
	<div class="container">
		<div class="row">
			<div class="fullwidth clearfix">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="flexslider clearfix" id="aboutslider">
						<ul class="slides">
							<?php  if(get_field('gallery')):
			
					while(the_repeater_field('gallery')):
						
						$noMR="";
					
						$url_imgBig = wp_get_attachment_image_src(get_sub_field('image'), 'lar_property');
						
					
						?>
							<li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class=""> <img alt="" src="<?php echo  $url_imgBig[0];?>"></li>
							<?php endwhile;
				endif;
			?>
						</ul>
						<!-- end slides -->
						<div class="aboutslider-shadow"> <span class="s1"></span> </div>
						<ul class="flex-direction-nav">
							<li><a href="#" class="flex-prev">Previous</a></li>
							<li><a href="#" class="flex-next flex-disabled">Next</a></li>
						</ul>
					</div>
					<!-- end slider --> 
				</div>
				<!-- end col-lg-6 -->
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="title1">
						<h3>
							<?php the_title(); ?>
						</h3>
						<hr>
					</div>
					<p class="lead">Cross Street: <?php echo get_field( "cross_street" ); ?></p>
					<p><?php echo get_field( "additional_information" ); ?></p>
					<div class="clearfix"></div>
					<hr>
					<ul class="product_details">
						<li class="version">Sq. Footage: <span><?php echo get_field( "sq_footage" ); ?></span></li>
						<li class="update">Status: <span><?php echo get_field( "status" ); ?></span></li>
						<li class="release">List Date: <span><?php echo get_field( "list_date" ); ?></span></li>
						<li class="release">Listed Days Back: <span><?php echo get_field( "day_back" ); ?></span></li>
						<li class="release">County: <span><?php echo get_field( "county" ); ?></span></li>
						<li class="release">Price: <span><b><?php echo get_field( "price" ); ?></b></span></li>
					</ul>
					<hr>
					<!--<a href="#map_google" class="btn btn-primary"><i class="fa fa-external-link"></i> View Map</a>--> 
				</div>
				<!-- end col-lg-4 -->
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="title1">
							<h3>Details</h3>
							<hr>
						</div>
						<ul class="product_details col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<li class="version">Location: <span><?php echo get_field( "sq_footage" ); ?></span></li>
							<li class="update">Area: <span><?php echo get_field( "status" ); ?></span></li>
							<li class="release">Price per sq. ft: <span><?php echo get_field( "list_date" ); ?></span></li>
							<li class="release">Year Built: <span><?php echo get_field( "day_back" ); ?></span></li>
							<li class="release">MLS#: <span><?php echo get_field( "county" ); ?></span></li>
						
						</ul>
						
						<ul class="product_details col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<li class="version">Cross Street: <span><?php echo get_field( "sq_footage" ); ?></span></li>
							<li class="update">Zoning: <span><?php echo get_field( "status" ); ?></span></li>
							<li class="release">Region: <span><?php echo get_field( "list_date" ); ?></span></li>
							<li class="release">Business Name: <span><?php echo get_field( "day_back" ); ?></span></li>
							<li class="release">Est Grs Annual Inc: <span><?php echo get_field( "county" ); ?></span></li>
														<li class="release">Est Net Income: <span><?php echo get_field( "county" ); ?></span></li>

							<li class="release">Est Total Annual Exp: <span><?php echo get_field( "county" ); ?></span></li>

						
						</ul>
						
							<div class="title1">
							<h3>Features</h3>
							<hr>
						</div>
						<ul class="product_details">
							<li class="version">Facility Description: <span>Outside Sign, Rest Room(s)-Private, Smoke Detector(s)</span></li>
							<li class="update">Includes: <span><?php echo get_field( "status" ); ?></span></li>
							<li class="release">Location: <span><?php echo get_field( "list_date" ); ?></span></li>
							<li class="release">Miscellaneous: <span><?php echo get_field( "day_back" ); ?></span></li>
							<li class="release">Parking/Garage: <span><?php echo get_field( "county" ); ?></span></li>
														<li class="release">Type 1: <span><?php echo get_field( "county" ); ?></span></li>

							<li class="release">Utilities: <span><?php echo get_field( "county" ); ?></span></li>

						
						</ul>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="title1">
							<h3>Map</h3>
							<hr>
						</div>
						<div class="googlemap">
							<?php 

$location = get_field('google_map');

if( !empty($location) ):
?>
							<div class="acf-map">
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<ul class="pager ">
					<li class="previous">
						<?php previous_post('%', '← Older Item', 'no'); ?>
					</li>
					<li class="next">
						<?php next_post('%', 'Newer Item →', 'no'); ?>
					</li>
				</ul>
			</div>
			<!-- end fullwidth --> 
		</div>
		<!-- end row -->
		
		<div class="authorbox_wrapper clearfix"> <img class="img-circle alignleft" width="100" src="<?php echo get_template_directory_uri(); ?>/demos/03_team.png" alt="">
			<h4>Posted by <a href="http://www.uprealproperty.com/about/">Thomas T. Nguyen</a></h4>
			<p>Broker/Owner-UP Real Estate & Property SRV<br/>
				BRE#  01316870 <a href="http://www.thomasnguyen5homes.com">http://www.thomasnguyen5homes.com</a><br/>
				2014 Auditor Officer Chinese Real Estates Of America - <a href="http://www.creaausa.org">http://www.creaausa.org</a> Tel:<a href="#">415-361-8535</a></p>
		</div>
		<!-- authorbox_wrapper --> 
	</div>
	<!-- end container --> 
</section>
<?php endwhile;endif;endif; ?>
<?php get_footer('no'); ?>
