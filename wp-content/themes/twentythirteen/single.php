<?php 
get_header('inpage'); 
?>	           

        <section class="post-wrapper-top dm-shadow clearfix">
            <div class="container">
                <div class="col-lg-12">
                    <h2>	<?php the_title(); ?></h2>
                     <!--<ul class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li>	<?php// the_title(); ?></li>
                    </ul>-->
                </div>
            </div>
        </section><!-- end post-wrapper-top -->
        
		<section class="postwrapper clearfix">
        	<div class="container">
            	<div class="row">
                	<div class="half-width clearfix">
							<?php if(is_single()): if (have_posts()) : while (have_posts()) : the_post(); ?> 

                    	<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                
                            <div class="blog_wrap single_wrap">
                            
                                <div class="title">
                                    <div class="post_date">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                    <h3><a href="single-blog-sidebar.html" title="">	<?php the_title(); ?></a></h3>
                                    <div class="post_meta">
                                        <span><i class="fa fa-calendar"></i> <?php the_time('l, F jS, Y') ?></span>
                                       
                                       
                                    </div><!-- end post-meta -->
                                </div><!-- end title -->
                                
								<div class="post_desc">
                               <?php the_content(); ?>
                                </div><!-- end post_desc -->
                                
                                <div class="authorbox_wrapper clearfix">
                                    <img class="img-circle alignleft" width="100" src="<?php echo get_template_directory_uri(); ?>/demos/03_team.png" alt="">  
                                    <h4>Posted by <a href="blog-author-details.html">Thomas T. Nguyen</a></h4>
                                    <p>Broker/Owner-UP Real Estate & Property SRV<br/>
									BRE#  01316870    <a href="http://www.thomasnguyen5homes.com">http://www.thomasnguyen5homes.com</a><br/>
									2014 Auditor Officer Chinese Real Estates Of America -  <a href="http://www.creaausa.org">http://www.creaausa.org</a>
									Tel:<a href="#">415-361-8535</a></p>
                                </div><!-- authorbox_wrapper -->
                            </div><!-- end blog-wrap -->
                                        
				
                            
          				</div><!-- end col-lg-12 -->
                        	<?php endwhile;endif;endif; ?>
						<div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                         	                             

             
                         	<div class="widget">
                            	<div class="title"><h3>NEWS</h3></div>
                                <ul class="recent_posts_widget">
								 <?php    
          $wp_query = new WP_Query();
             $param = array(
                 'posts_per_page' => '7',
                 'post_type' => 'post',
                 'post_status' => 'publish',
               'paged' => $paged,
                 'order' => 'DESC',
             );
             $wp_query->query($param);
				 $i=0;
             if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post();
			 $i++;
             $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'thum');
			if($thumb_url[0]==""){
				$get_img = "http://www.uprealproperty.com/no-photo.jpg";
			}
			
			else{
				$get_img = $thumb_url[0];
			}
			
         ?>
								
                                    <li>
                                        <a href="<?php the_permalink();?>">
                                        <img src="<?php echo $get_img;?>" alt="" />  <?php if(mb_strlen($post->post_title)>45) { $cont= mb_substr($post->post_title,0,45) ; echo strip_tags($cont. ･･･ );} else {echo strip_tags($post->post_title);}?>
                                        </a>
                                        <a class="readmore" href="<?php the_permalink();?>"><?php the_time('F j, Y') ?></a>
                                    </li>
                                    <?php endwhile;endif;?>
                                </ul><!-- recent posts -->                              
                            </div><!-- end widget -->

                         	                                                                  
                        </div><!-- end sidebar -->
                	</div><!-- end half-width -->
                </div><!-- end row -->
            </div><!-- end container -->
			
			

        </section><!-- end postwrapper -->

        <?php get_footer('no'); ?>