<?php 
get_header('inpage'); 
?>             

        <section class="post-wrapper-top dm-shadow clearfix">
            <div class="container">
                <div class="col-lg-12">
                    <h2><?php single_cat_title(); ?></h2>
                     <!--<ul class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li>  <?php// the_title(); ?></li>
                    </ul>-->
                </div>
            </div>
        </section><!-- end post-wrapper-top -->
        
  <section class="whitewrapper bordertop greybg clearfix">
          <div class="container">
              <div class="row">
                  <div class="fullwidth clearfix">
                      <div class="col-lg-12">
                    
                           <div id="bbpress-forums">
                                <ul class="bbp-forums">
                                    <li class="bbp-header">
                                        <ul class="forum-titles">
                                            <li class="bbp-forum-info">Title</li>
                                            <li class="bbp-forum-topic-count">Sq. Footage</li>
                                            <li class="bbp-forum-reply-count">ASKING</li>
                                            <li class="bbp-forum-freshness">Contacts</li>
                                        </ul>
                                    </li><!-- .bbp-header -->
                            
                                
                            <?php 
	
		  $term_slug = get_query_var( 'term' );		 
		   $taxonomy = 'listings';
		   $category = get_term_by('slug',$term_slug,$taxonomy);				

		   $wp_query3 = new WP_Query();
		   $param=array(
				   'post_type'=>'propeties',
				   'order' => 'DESC',
				   'posts_per_page' => '-1',
				   'tax_query' => array(
					   array(
						   'taxonomy' => $taxonomy,
						   'field' => 'slug',
						   'terms' => $term_slug
						   )
					   )
				   );
				
			$wp_query3->query($param);
			if($wp_query3->have_posts()): while($wp_query3->have_posts()) : $wp_query3->the_post();
			 $thumbID = get_post_thumbnail_id($post->ID);
            $thumb_url = wp_get_attachment_image_src($thumbID,'smal_property');
		?>
                                 
                                     
                                    <li class="bbp-body">   
                                        <ul class="forum type-forum status-publish hentry loop-item-0 odd bbp-forum-status-open bbp-forum-visibility-publish">
                                            <li class="bbp-forum-info">
                                                <img class="floatL" src="<?php echo  $thumb_url[0];?>"/>

                                               <div class="floatL descipt"> <a title="General" href="#" class="bbp-forum-title"><?php the_title();?></a>
                                                <div class="bbp-forum-content"><?php echo get_field( "additional_information" ); ?></div>
                                                </div>
                                            </li>
                                            <li class="bbp-forum-topic-count"><?php echo get_field( "sq_footage" ); ?></li>
                                            <li class="bbp-forum-reply-count"><?php echo get_field( "asking" ); ?></li>
                                            <li class="bbp-forum-freshness fz12">
                                              <?php $object_p = get_field( "contact" ); 
											//  var_dump($object_p);
											  			foreach( $object_p as $post2) {
															echo $post2->post_title;
														}
											  
											  ?>

                                            <a href="<?php echo post_permalink( $post->ID ); ?> "><button type="button" class="btn btn-info view-pro">View details</button></a>

                                              
                                            </li>
                                        </ul><!-- end bbp forums -->
                                     </li>


<?php endwhile; endif; wp_reset_query(); ?>

                                </ul><!-- .forums-directory -->
                            </div> <!-- /bbpress -->

              <div class="clearfix"></div>
                                                        
                         <!--   <div class="pagination_wrapper text-center">
                                <!-- Pagination Normal 
                                <ul class="pagination">
                                  <li><a href="#">«</a></li>
                                  <li class="active"><a href="#">1</a></li>
                                  <li><a href="#">2</a></li>
                                  <li><a href="#">3</a></li>
                                  <li class="disabled"><a href="#">»</a></li>
                                </ul>
                            </div>-->
                            
                  </div><!-- end col-lg-12 -->
                  </div><!-- end fullwidth -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>

        <?php get_footer('no'); ?>